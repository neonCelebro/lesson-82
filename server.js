const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const config = require('./config');

const port = 8000;
const app = express();

app.use(cors());
app.use(express.json());

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
    app.use('/artists', artists());
    app.use('/albums', albums());
    app.use('/tracks', tracks());
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
