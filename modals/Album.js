const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const albumSchema = new Schema({
    name: {type: String, required : true, unique: true},
    author: {type: Schema.Types.ObjectId, ref: 'Artist', required: true},
    year: {type: Number, required : true},
    image: {type: String},
    info: {type: String}
});

const Album = mongoose.model('Album', albumSchema);

module.exports = Album;